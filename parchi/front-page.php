<?php
/**
 * The home page template
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package parchi
 */

get_header();
?>

<!-- Contenuto Home Page Parchi Tematici -->
<div class="container" >
    <h2 class="text-center" style="color:#4286f4;">Il più grande portale dei Parchi della Riviera!</h2>
    <br>
    <div class="row">
    <?php

        $query = new WP_Query( array(
            'post_type' => 'parchi'
        ) );
        //contatore per i div dei post
        $numero_post = 0;

        if ( $query->have_posts() ) {
            //per ogni parco inserisco immagine, nome e descrizione
            while ( $query->have_posts() ) {
                $numero_post++;
                ?>
            
                
                    <div class="parchi col-xs-12 col-md-4" >
                    <?php $query->the_post(); ?>

                        <a href="<?php echo(get_the_permalink()); ?>"><?php the_post_thumbnail( 'medium' ); ?></a>
                        <br>
                        <h3><a href="<?php echo(get_the_permalink()); ?>" style="color:#ffb135;font-weight:bold;"><?php the_title(); ?></a></h3>
                        <div class="descrizione" style="min-height: 100px;">
                            <p><?php the_excerpt(); ?></p>
                        </div>
                        <div class="select">
                            <input class="form-check-input" type="checkbox" name="parco" value="<?php the_title(); ?>" onclick = "mostraSelezione(this)"><span> Seleziona</span>
                        </div>
                    </div>
                    

                <?php
                //Dopo il terzo post chiudo la riga e apro quella successiva
                if ( $numero_post == 3 ) {
                    echo "</div>"; //chiusura div "row"
                    echo "<br>";
                    echo "<div class='row'>"; //apertura div "row" successivo
                }

            } // end while
        } // end if
    ?>
    </div>
    <!-- Popup per mostrare i Parchi selezionati -->
    <div id="popup">
        <h2 class="text-center">Parchi selezionati: <span id="numeroselezione"></span></h2>
        <p class="text-center">Invia richiesta per avere informazioni sui parchi selezionati!  <button type="submit" class="btn btn-warning" style="align:center;"><a href="http://localhost/sito_parchi_tematici/contatti/" style="color:white;">Richiedi informazioni</a></button></p>
    </div>
</div>

<?php
get_footer();