<?php
/**
 * parchi functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package parchi
 */

if ( ! function_exists( 'parchi_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function parchi_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on parchi, use a find and replace
		 * to change 'parchi' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'parchi', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'parchi' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'parchi_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'parchi_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function parchi_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'parchi_content_width', 640 );
}
add_action( 'after_setup_theme', 'parchi_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function parchi_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'parchi' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'parchi' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'parchi_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function parchi_scripts() {
	wp_enqueue_style( 'parchi-style', get_stylesheet_uri() );

	wp_enqueue_script( 'parchi-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'parchi-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'parchi_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//funzione per inserire Google Fonts nel file "header.php"
function inserisci_google_fonts(){
    echo "<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet'>";
}
add_action('wp_head', 'inserisci_google_fonts');

//registrazione Custom Post "Parchi"
register_post_type( 'parchi', array(
	'label' => 'Parchi',
	'public' => true,
	'menu_icon' => 'dashicons-palmtree',
	'supports' => array('title', 'editor', 'thumbnail', 'excerpt')	
	) );

//PLUGIN "cmb2"
//******************** */
add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_sample_metaboxes() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_yourprefix_';

	/**
	 * Initiate the metabox
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'test_metabox',
		'title'         => __( 'Informazioni sul parco', 'cmb2' ),
		'object_types'  => array( 'parchi', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );

	// Regular text field "INDIRIZZO"
	$cmb->add_field( array(
		'name'       => __( 'Indirizzo', 'cmb2' ),
		'desc'       => __( 'Inserisci indirizzo del parco', 'cmb2' ),
		'id'         => $prefix . 'indirizzo',
		'type'       => 'text',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );

	// Regular text field "TELEFONO"
	$cmb->add_field( array(
		'name'       => __( 'Telefono', 'cmb2' ),
		'desc'       => __( 'Inserisci numero telefono del parco', 'cmb2' ),
		'id'         => $prefix . 'telefono',
		'type'       => 'text',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );


	// URL text field "WEBSITE"
	$cmb->add_field( array(
		'name' => __( 'URL Sito Web', 'cmb2' ),
		'desc' => __( 'Inserisci indirizzo sito web del parco', 'cmb2' ),
		'id'   => $prefix . 'url',
		'type' => 'text',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );

	// Email text field "EMAIL"
	$cmb->add_field( array(
		'name' => __( 'Email', 'cmb2' ),
		'desc' => __( 'Inserisci indirizzo email del parco', 'cmb2' ),
		'id'   => $prefix . 'email',
		'type' => 'text_email',
		// 'repeatable' => true,
	) );

	
	//Custom field per ORARI DI APERTURA
	$cmb->add_field( array(
		'name'    => 'Periodo e orari di apertura',
		'desc'    => 'Inserisci immagine orari di apertura',
		'id'      => 'field_image',
		'type'    => 'file',
		// Optional:
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'text'    => array(
			'add_upload_file_text' => 'Aggiungi immagine' // Change upload button text. Default: "Add or Upload File"
		),
		// query_args are passed to wp.media's library query.
		'query_args' => array(
			'type' => 'application/pdf', // Make library only display PDFs.
			
			'type' => array(
				'image/gif',
			 	'image/jpeg',
			 	'image/png',
			 ),
		),
		'preview_size' => 'medium', // Image size to use when previewing in the admin.
	) );

	//Editor per TARIFFE INDIVIDUALI
	$cmb->add_field( array(
		'name'    => 'Tariffe individuali',
		'desc'    => 'Inserisci le tariffe individuali del parco',
		'id'      => 'tariffe_wysiwyg',
		'type'    => 'wysiwyg',
		'options' => array(),
	) );

	//Editor per TARIFFE GRUPPI
	$cmb->add_field( array(
		'name'    => 'Tariffe gruppi',
		'desc'    => 'Inserisci le tariffe gruppi del parco',
		'id'      => 'gruppi_wysiwyg',
		'type'    => 'wysiwyg',
		'options' => array(),
	) );

}


//inserimento CUSTOM TAXONOMIES per elencare SERVIZI dei parchi
add_action( 'init', 'create_parchi_taxonomies', 0 );

function create_parchi_taxonomies(){
$labels = array(
	'name'              => _x( 'Servizi', 'taxonomy general name', 'textdomain' ),
	'singular_name'     => _x( 'Servizio', 'taxonomy singular name', 'textdomain' ),
	'search_items'      => __( 'Cerca servizi', 'textdomain' ),
	'all_items'         => __( 'Tutti i servizi', 'textdomain' ),
	// 'parent_item'       => __( 'Parent Genre', 'textdomain' ),
	// 'parent_item_colon' => __( 'Parent Genre:', 'textdomain' ),
	'edit_item'         => __( 'Modifica servizio', 'textdomain' ),
	'update_item'       => __( 'Aggiorna servizio', 'textdomain' ),
	'add_new_item'      => __( 'Aggiungi servizio', 'textdomain' ),
	'new_item_name'     => __( 'Nome del nuovo servizio', 'textdomain' ),
	'menu_name'         => __( 'Servizi', 'textdomain' ),
);

$args = array(
	'hierarchical'      => true,
	'labels'            => $labels,
	'show_ui'           => true,
);

register_taxonomy( 'servizi', 'parchi', $args );
}






