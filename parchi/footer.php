<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package parchi
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="container-fluid" style="background-color:#666560; min-height: 200px;margin-bottom:0px;">
			
			<div class="container" style="color:white;">
				<!-- Info Contatti e Social -->
				<div class="row">
					<!-- Contatti -->
					<div class="col-xs-12 col-md-6">
						<h3>IFTS Communication</h3>
						<p><span class="glyphicon glyphicon-map-marker" style="color:#ffb135;"></span> Via Valturio, 100 - 47921 Rimini (RN)</p>
						<p><span class="glyphicon glyphicon-earphone" style="color:#ffb135;"></span> 0541 200000</p>
						<p><span class="glyphicon glyphicon-envelope" style="color:#ffb135;"></span> info@ifts.com</p>
					</div>
					<!-- Social -->
					<div class="col-xs-12 col-md-6">
						<h3>SEGUICI SUI SOCIAL</h3>
						<div class="social">
							<?php echo do_shortcode('[ssba-buttons]'); ?>
        				</div>
					</div>
				</div>

				<!-- Info Copyright -->
				<div class="site-info text-center">
					<p>Copyright © 2018 IFTS Communication. All rights reserved.</p>
				</div>
			</div>

		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
