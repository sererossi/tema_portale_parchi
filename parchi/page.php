<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package parchi
 */

get_header();
?>
<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
		?>
			<h3><?php the_title(); ?></h3>
			
			<?php
			the_content();

		endwhile; // End of the loop.

		?>


		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php
//get_sidebar();
get_footer();
