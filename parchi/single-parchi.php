<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package parchi
 */

get_header();

the_post();

?>
    <!-- Contenuto del post -->
    <div class="parchi-content container">
        <div class="row">
            <div class="colonna1 col-xs-12 col-md-6">

                <!-- Immagine parco -->
                <div class="thumbnail">
                    <?php the_post_thumbnail( 'full' ); ?>
                </div>

                <!-- Nome parco -->
                <h2 style="color: #3399ff; font-weight: bold;">
                    <?php the_title(); ?>
                </h2>

                <!-- Descrizione parco -->
                <div class="post_content">
                    <?php the_content(); ?>
                </div>

                <!-- Box periodo apertura del parco -->
                <div class="apertura">
                    <h3 style="color: #3399ff; font-weight: bold;">Periodo e orari di apertura</h3>
                    <div class="img_orari thumbnail">
                        <?php
                            echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'field_image_id', 1 ), 'large');
                            
                        ?>
                    </div>          
                </div>

                <!-- Box tariffe individuali -->
                <div class="tariffe">
                    <h3 style="color: #3399ff; font-weight: bold;">Tariffe individuali</h3>
                    <?php
                    echo wpautop( get_post_meta( get_the_ID(), 'tariffe_wysiwyg', true ) );
                    ?>          
                </div>

                <!-- Box tariffe gruppi -->
                <div class="tariffe">
                    <h3 style="color: #3399ff; font-weight: bold;">Tariffe gruppi</h3>
                    <?php
                    echo wpautop( get_post_meta( get_the_ID(), 'gruppi_wysiwyg', true ) );
                    ?>          
                </div>


            </div>

            <div class="colonna2 col-xs-12 col-md-6">

                <!-- Box dei contatti del parco -->
                <div class="contatti">
                    <h3 style="color: #3399ff; font-weight: bold;">Contatti</h3>
                    <?php
                        $indirizzo  = get_post_meta( get_the_ID(), '_yourprefix_indirizzo', true );
                        $telefono  = get_post_meta( get_the_ID(), '_yourprefix_telefono', true );
                        $email = get_post_meta( get_the_ID(), '_yourprefix_email', true );
                        $url   = get_post_meta( get_the_ID(), '_yourprefix_url', true );
                    ?>
                    <p><span class="glyphicon glyphicon-map-marker" style="color:#ffb135;"></span> <?php echo esc_html( $indirizzo ); ?></p>    
                    <p><span class="glyphicon glyphicon-earphone" style="color:#ffb135;"></span> <?php echo esc_html( $telefono ); ?></p>
                    <p><span class="glyphicon glyphicon-envelope" style="color:#ffb135;"></span> <?php echo is_email( $email ); ?></p>    
                    <a href="http://<?php echo esc_html( $url ); ?>"><span class="glyphicon glyphicon-globe" style="color:#ffb135;"></span> <?php echo esc_html( $url ); ?></a>  
                    
                </div>

                <!-- Box Photogallery -->
                <div class="photogallery">
                    <h3 style="color: #3399ff; font-weight: bold;">Photogallery</h3>
                    <?php
                        $id = get_the_ID();
                        switch ($id) {
                            case 27:
                                echo do_shortcode('[Best_Wordpress_Gallery id="2" gal_title="Foto Italia in Miniatura"]');
                                break;
                            case 16:
                                echo do_shortcode('[Best_Wordpress_Gallery id="3" gal_title="Foto Mirabilandia"]');
                                break;
                            case 26:
                                echo do_shortcode('[Best_Wordpress_Gallery id="7" gal_title="Foto Fiabilandia"]');
                                break;
                            case 25:
                                echo do_shortcode('[Best_Wordpress_Gallery id="5" gal_title="Foto Acquario di Cattolica"]');
                                break;
                            case 24:
                                echo do_shortcode('[Best_Wordpress_Gallery id="6" gal_title="Foto Aquafan"]');
                                break;
                            case 18:
                                echo do_shortcode('[Best_Wordpress_Gallery id="4" gal_title="Foto Oltremare"]');
                                break; 
                        }
                     ?>
                </div>

                <!-- Box SERVIZI -->
                    <div class="disabili">
                        <h3 style="color: #3399ff; font-weight: bold;">Servizi</h3>
                        <?php 
                            //leggo e stampo tutti i servizi
                            $terms = wp_get_post_terms(get_the_id(), 'servizi');

                            foreach ($terms as $term){
                                echo '<p><span class="glyphicon glyphicon-ok" style="color:#ffb135;"></span> '.$term->name . '</p><br>';
                            }
                            
                        ?>
                    </div>
                        
                <br>
                <br>
                
            </div> 

        </div>
        <!-- Box condivisione social -->        
        <div class="social">
                    <p>Condividi questo post</p>
                    <?php echo do_shortcode('[ssba-buttons]'); ?>
        </div>


    </div>

<?php
get_footer();