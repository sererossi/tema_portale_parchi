<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package parchi
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!-- link CDN css Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<?php wp_head(); ?>
	<!-- link CDN script javascript Bootstrap -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		var numero = 0;
		function mostraSelezione(elemento){
			if (elemento.checked) {
				numero += 1;
			} else {
				numero -= 1;
			}

			document.getElementById('popup').style.display="block";
			document.getElementById('numeroselezione').innerHTML = numero;
			
			if (numero == 0) {
				document.getElementById('popup').style.display="none";
			}
		}


	</script>
	<style>
		#popup{
			width: 100%;
			height: auto;
			background: #385dff;
			padding: 10px 20px;
			color: rgb(255, 255, 255);
			position: sticky;
			display: none;
			bottom: 50px;
		}

	</style>
</head>

<body <?php body_class(); ?> style="min-height: 100%;">
<div id="page" class="site">

	<!-- HEADER -->
	<header id="masthead" class="site-header">
	<!-- menu di navigazione -->	
	<!-- Inizio -->		
	<nav class="navbar" style="background-color: #ffb135;">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="glyphicon glyphicon-menu-hamburger" style="color:white;"></span>	                        
				</button>
				<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" style="color: white;font-weight: bold;">IFTS Communication</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar" style="font-weight: bold;">
				<?php wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'container'      => false,
					'items_wrap'     => '<ul id="%1$s" class="nav navbar-nav">%3$s</ul>'
					) 
				); 
				?>
			</div>
		</div>
	</nav> 
	<!-- End -->

	</header>
	<!-- / HEADER -->

	<div id="content" class="site-content">
